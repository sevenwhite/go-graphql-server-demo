package main

import (
	"github.com/graphql-go/graphql"
)

const AdminUserRole = "admin"

const GuestUserRole = "guest"

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Role string `json:"role"`
}

var userRoleType = graphql.NewEnum(
	graphql.EnumConfig{
		Name:        "UserRole",
		Description: "The role of user",
		Values: graphql.EnumValueConfigMap{
			AdminUserRole: &graphql.EnumValueConfig{
				Value: AdminUserRole,
			},
			GuestUserRole: &graphql.EnumValueConfig{
				Value: GuestUserRole,
			},
		},
	},
)

var userType = graphql.NewObject(
	graphql.ObjectConfig{
		Name:        "User",
		Description: "An user entity",
		Fields: graphql.Fields{
			"id": &graphql.Field{
				Type: graphql.Int,
			},
			"name": &graphql.Field{
				Type: graphql.String,
			},
			"role": &graphql.Field{
				Type: userRoleType,
			},
		},
	},
)
