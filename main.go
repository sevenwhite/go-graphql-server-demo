package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/graphql-go/graphql"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

var Schema graphql.Schema
var currentUser = User{ID: 1, Name: "SevenWhite", Role: AdminUserRole}
var ruslan = User{ID: 2, Name: "Ruslan", Role: GuestUserRole}
var valera = User{ID: 3, Name: "Valera", Role: GuestUserRole}
var users = []User{currentUser, ruslan, valera}

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/ping", ping)
	r.Get("/graphql", graphqlHandler)
	r.Post("/graphql", graphqlPostHandler)

	err := http.ListenAndServe(":"+port, r)
	if err != nil {
		fmt.Println(err)
	}
}

func graphqlPostHandler(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Error while reading body: %v", err)
		http.Error(w, "can't read body", http.StatusBadRequest)
		return
	}

	query := &GraphQLRequest{}
	err = json.Unmarshal(body, query)
	if err != nil {
		log.Printf("Error while unmarshaling body: %v", err)
		http.Error(w, "can't unmarshal body", http.StatusBadRequest)
		return
	}

	result := graphql.Do(graphql.Params{
		Schema:         Schema,
		RequestString:  query.Query,
		VariableValues: query.Variables,
		Context:        context.WithValue(context.Background(), "currentUser", currentUser),
	})

	if len(result.Errors) > 0 {
		log.Printf("wrong result, unexpected errors: %v", result.Errors)
		return
	}
	json.NewEncoder(w).Encode(result)
}

func graphqlHandler(w http.ResponseWriter, r *http.Request) {
	result := graphql.Do(graphql.Params{
		Schema:        Schema,
		RequestString: r.URL.Query().Get("query"),
		Context:       context.WithValue(context.Background(), "currentUser", currentUser),
	})

	if len(result.Errors) > 0 {
		log.Printf("wrong result, unexpected errors: %v", result.Errors)
		return
	}
	json.NewEncoder(w).Encode(result)
}

func init() {
	var err error
	Schema, err = graphql.NewSchema(
		graphql.SchemaConfig{
			Query:    queryType,
			Mutation: mutationType,
		},
	)
	if err != nil {
		log.Fatalf("failed to create schema, error: %v", err)
	}
}

func ping(w http.ResponseWriter, _ *http.Request) {
	w.Write([]byte("pong"))
}
