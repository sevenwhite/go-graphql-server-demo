package main

import (
	"github.com/graphql-go/graphql"
	"github.com/pkg/errors"
)

var mutationType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Mutation",
		Fields: graphql.Fields{
			"createUser": &graphql.Field{
				Type:        userType,
				Description: "Create new user",
				Args: graphql.FieldConfigArgument{
					"name": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(graphql.String),
					},
					"role": &graphql.ArgumentConfig{
						Type: graphql.NewNonNull(userRoleType),
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					name, isOk := p.Args["name"].(string)
					if !isOk {
						return User{}, errors.New("The name is required")
					}
					role, isOk := p.Args["role"].(string)
					if isOk {
						return User{}, errors.New("The role is required")
					}
					newId := len(users) + 1
					newUser := User{ID: newId, Name: name, Role: role}
					users = append(users, newUser)
					return newUser, nil
				},
			},
		},
	},
)
