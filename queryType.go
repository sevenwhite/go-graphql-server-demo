package main

import (
	"github.com/graphql-go/graphql"
)

var queryType = graphql.NewObject(
	graphql.ObjectConfig{
		Name: "Query",
		Fields: graphql.Fields{
			"me": &graphql.Field{
				Type: userType,
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					return p.Context.Value("currentUser"), nil
				},
			},
			"users": &graphql.Field{
				Type:        graphql.NewList(userType),
				Description: "List of users",
				Args: graphql.FieldConfigArgument{
					"role": &graphql.ArgumentConfig{
						Description: "The role of the user",
						Type:        userRoleType,
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					userRole, isOk := p.Args["role"].(string)
					if isOk {
						if userRole == AdminUserRole || userRole == GuestUserRole {
							usersByRole := []User{}
							for _, user := range users {
								if user.Role == userRole {
									usersByRole = append(usersByRole, user)
								}
							}
							return usersByRole, nil
						}

						return []User{}, nil
					}

					return users, nil
				},
			},
			"user": &graphql.Field{
				Type:        userType,
				Description: "Get user",
				Args: graphql.FieldConfigArgument{
					"name": &graphql.ArgumentConfig{
						Description: "The name of user",
						Type:        graphql.String,
					},
					"id": &graphql.ArgumentConfig{
						Type:        graphql.Int,
						Description: "The id of user",
					},
				},
				Resolve: func(p graphql.ResolveParams) (interface{}, error) {
					userId, isOk := p.Args["id"].(int)
					if isOk {
						for _, user := range users {
							if user.ID == userId {
								return user, nil
							}
						}
					}

					userName, isOk := p.Args["name"].(string)
					if isOk {
						for _, user := range users {
							if user.Name == userName {
								return user, nil
							}
						}
					}

					return User{}, nil
				},
			},
		},
	},
)
